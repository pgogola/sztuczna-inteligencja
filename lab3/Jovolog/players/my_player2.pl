/* -*- mode: Prolog; comment-column: 48 -*- */

/****************************************************************************
 *
 * Copyright (c) 2013 Witold Paluszynski
 *
 * I grant everyone the right to copy this program in whole or part, and
 * to use it for any purpose, provided the source is properly acknowledged.
 *
 * Udzielam kazdemu prawa do kopiowania tego programu w calosci lub czesci,
 * i wykorzystania go w dowolnym celu, pod warunkiem zacytowania zrodla.
 *
 ****************************************************************************/


/*
  This program implements a simple agent strategy for the wumpus world.
  The agent ignores all dangers of the wumpus world.
  The strategy is to go forward along the perimeter,
  turn left when reached the opposing wall,
  but first of all pick up gold if stumbled upon it,
  and exit the game if at home with gold.
  This version registers all steps on a stack, and uses it to reverse
  the actions after having found gold, thus properly returning home.

  Also demonstrates how to keep track of her own position and orientation.
  The agent assumes that the starting point is (1,1) and orientation "east".
*/

% auxiliary initial action generating rule
act(Action, Knowledge) :-

	% To avoid looping on act/2.
	not(gameStarted),
	assert(gameStarted),

	% Creating initial knowledge
	worldSize(X,Y),				%this is given
	assert(myWorldSize(X,Y)),
	assert(myPosition(1, 1, east)),		%this we assume by default
	assert(myTrail([])),
	assert(visited([])),
	assert(breezeFields([])),
	assert(stenchFields([])),
	assert(haveGold(0)),
	act(Action, Knowledge).

% standard action generating rules
% this is our agent's algorithm, the rules will be tried in order
act(Action, Knowledge) :- exit_if_home(Action, Knowledge). %if at home with gold
act(Action, Knowledge) :- go_back_step(Action, Knowledge). %if have gold elsewhere
act(Action, Knowledge) :- pick_up_gold(Action, Knowledge). %if just found gold
act(Action, Knowledge) :- go_back_if_breeze(Action, Knowledge). %if breeze prercept, then go back
act(Action, Knowledge) :- else_move_on(Action, Knowledge). %otherwise
act(Action, Knowledge) :- turn_if_next_visited(Action, Knowledge).
act(Action, Knowledge) :- go_back(Action, Knowledge).
act(Action, Knowledge) :- turn_back(Action, Knowledge). %if all visited, then go back
act(Action, Knowledge) :- turn_if_wall(Action, Knowledge). %if against the wall

exit_if_home(Action, Knowledge) :-
	haveGold(NGolds), NGolds > 0,
	myPosition(1, 1, Orient),
	Action = exit,				%done game
	Knowledge = [].				%irrelevant but required

go_back_step(Action, Knowledge) :-
	%%% assuming we have just found gold:
	%%% 1. our last action must have been grab
	%%% 2. our previuos action must have been moveForward
	%%% 3. so we are initiating a turnback and then return:
	%%%    (a) pop grab from the stack
	%%%    (b) replace it by an artificial turnRight we have never
	%%%        executed, but we will be reversing by turning left
	%%%    (c) execute a turnRight now which together will turn us back
	%%% 4. after that we are facing back and can execute actions in reverse
	%%% 5. because of grab we can be sure this rule is executed exactly once
	haveGold(NGolds), NGolds > 0,
	myWorldSize(Max_X, Max_Y),
	myTrail(Trail),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	visited(Visited),

	Trail = [ [grab,X,Y,Orient] | Trail_Tail ],
	New_Trail = [ [turnRight,X,Y,Orient] | Trail_Tail ], %Orient is misleading here
	Action = turnLeft,
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, Orient),
					myTrail(New_Trail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].

go_back_step(Action, Knowledge) :-
	haveGold(NGolds), NGolds > 0,
	myWorldSize(Max_X, Max_Y),
	myTrail([ [Action,X,Y,Orient] | Trail_Tail ]),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	Action = moveForward,
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, Orient),
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].

%%% backtracking a step can be moving or can be turning
go_back_step(Action, Knowledge) :- go_back_turn(Action, Knowledge).

go_back_turn(Action, Knowledge) :-
	haveGold(NGolds), NGolds > 0,
	myWorldSize(Max_X, Max_Y),
	myTrail([ [OldAct,X,Y,Orient] | Trail_Tail ]),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	%% if our previous action was a turn, we must reverse it now
	((OldAct=turnLeft,Action=turnRight);(OldAct=turnRight,Action=turnLeft)),
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, Orient),
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].

pick_up_gold(Action, Knowledge) :-
	glitter,
	Action = grab,			    %this is easy, we are sitting on it
	haveGold(NGolds),		    %we must know how many golds we have
	NewNGolds is NGolds + 1,
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail(Trail),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	visited(Visited),
	appendIfNotMember([X, Y], Visited, New_Visited),
	New_Trail = [ [Action,X,Y,Orient] | Trail ], %important to remember grab
	Knowledge = [	gameStarted,
		            haveGold(NewNGolds),
			     	myWorldSize(Max_X, Max_Y),
			     	myPosition(X, Y, Orient),	%the position stays the same
			     	myTrail(New_Trail),
			     	visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].


go_back(Action, Knowledge) :- go_back_step_turn_after_breeze(Action, Knowledge).
go_back(Action, Knowledge) :- go_back_step_after_breeze(Action, Knowledge).
go_back_if_breeze(Action, Knowledge) :- go_back_if_breeze_step(Action, Knowledge).
go_back_if_breeze(Action, Knowledge) :- go_back_if_breeze_turn(Action, Knowledge).

go_back_step_after_breeze(Action, Knowledge) :-
	myWorldSize(Max_X, Max_Y),
	myTrail([ [Action,Old_X,Old_Y,_] | Trail_Tail ]),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	visited(Visited),
	myPosition(X, Y, Orient),
	forwardStep(X, Y, Orient, New_X, New_Y),
	haveGold(NGolds),
    Old_X = New_X, Old_Y = New_Y,
	Action = moveForward,
	Knowledge = [	gameStarted,
		            haveGold(NGolds),
				    myWorldSize(Max_X, Max_Y),
				    myPosition(New_X, New_Y, Orient),
				    myTrail(Trail_Tail),
				    visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].

go_back_step_turn_after_breeze(Action, Knowledge) :-
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail([ [OldAct,X,Y,OldOrient] | Trail_Tail ]),
	((OldAct=turnLeft,Action=turnRight);(OldAct=turnRight,Action=turnLeft);(OldAct=moveForward,Action=moveForward)),
	shiftOrientTurn(Orient, Action, NewOrient),
	%% member([X, Y], BreezeFields),
	visited(Visited),
	Knowledge = [	gameStarted,
	             	haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),	%the position stays the same
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].

go_back_if_breeze_step(Action, Knowledge) :-
	((breeze,Percept1=breeze);Percept1=none),
	((stench,Percept2=stench);Percept2=none),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail([ [OldAct,X,Y,OldOrient] | Trail_Tail ]),
	((OldAct=turnLeft,Action=turnRight);(OldAct=turnRight,Action=turnLeft);(OldAct=moveForward,Action=moveForward)),
	shiftOrientTurn(Orient, Action, NewOrient),
	((Percept1=breeze,member([X, Y], BreezeFields));(Percept2=stench,member([X, Y], StenchFields))),
	visited(Visited),
	Knowledge = [	gameStarted,
	             	haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),	%the position stays the same
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].

go_back_if_breeze_turn(Action, Knowledge) :-
	((breeze,Percept1=breeze);Percept1=none),
	((stench,Percept2=stench);Percept2=none),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	shiftOrientTurn(Orient, turnLeft, NewOrient),
	myTrail(Trail),
	visited(Visited),
	not(member([X, Y], BreezeFields)),
	not(member([X, Y], StenchFields)),
	New_Trail = [ [turnRight,X,Y,Orient] | Trail ], %Orient is misleading here
	Action = turnLeft,
	((Percept1=breeze,appendIfNotMember([X, Y], BreezeFields, New_BreezeFields));New_BreezeFields=BreezeFields),
	((Percept2=stench,appendIfNotMember([X, Y], StenchFields, New_StenchFields));New_StenchFields=StenchFields),
	(not(New_BreezeFields=BreezeFields);not(New_StenchFields=StenchFields)),
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
	             	haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),	%the position stays the same
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(New_BreezeFields),
					stenchFields(New_StenchFields)].

turn_if_wall(Action, Knowledge) :-
	myPosition(X, Y, Orient),
	myWorldSize(Max_X,Max_Y),
	againstWall(X, Y, Orient, Max_X, Max_Y),
	Action = turnRight,			%always successful
	shiftOrientTurn(Orient, Action, NewOrient),		%always successful
	haveGold(NGolds),
	myTrail(Trail),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].

againstWall(X, Y, Orient, Max_X, Max_Y) :- X = Max_X, Orient = east.
againstWall(X, Y, Orient, Max_X, Max_Y) :- Y = 1, Orient = south.

againstWall(X, Y, Orient, Max_X, Max_Y) :- Y = Max_Y, Orient = north.
againstWall(X, Y, Orient, Max_X, Max_Y) :- X = Max_X, Orient = east.

againstWall(X, Y, Orient, Max_X, Max_Y) :- X = 1, Orient = west.
againstWall(X, Y, Orient, Max_X, Max_Y) :- Y = Max_Y, Orient = north.

againstWall(X, Y, Orient, Max_X, Max_Y) :- Y = 1, Orient = south.
againstWall(X, Y, Orient, Max_X, Max_Y) :- X = 1, Orient = west.

shiftOrient(east, north).
shiftOrient(north, west).
shiftOrient(west, south).
shiftOrient(south, east).

else_move_on(Action, Knowledge) :-
	Action = moveForward,			%this will fail on a wall
	haveGold(NGolds),
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),
	forwardStep(X, Y, Orient, New_X, New_Y),
	myTrail(Trail),
	visited(Visited),
	not(member([New_X,New_Y], Visited)),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	not(againstWall(X, Y, Orient, Max_X, Max_Y)),
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(New_X, New_Y, Orient),
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].

forwardStep(X, Y, east,  New_X, Y) :- New_X is (X+1).
forwardStep(X, Y, south, X, New_Y) :- New_Y is (Y-1).
forwardStep(X, Y, west,  New_X, Y) :- New_X is (X-1).
forwardStep(X, Y, north, X, New_Y) :- New_Y is (Y+1).


turn_if_next_visited(Action, Knowledge) :- turn_left_if_visited(Action, Knowledge).
turn_if_next_visited(Action, Knowledge) :- turn_right_if_visited(Action, Knowledge).

turn_left_if_visited(Action, Knowledge) :- 
	(not(breeze),not(stench)),
	Action = turnLeft,
	haveGold(NGolds),
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),
	shiftOrientTurn(Orient, Action, New_Orient),
	forwardStep(X, Y, New_Orient, New_X, New_Y),
	not(againstWall(X, Y, New_Orient, Max_X, Max_Y)),
	myTrail(Trail),
	visited(Visited),
	not(member([New_X,New_Y], Visited)),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	[[_,_,_,OldOrient] | _ ] = Trail,
	swapTrailTurn(Orient, OldOrient, Action, TrailAction),
	New_Trail = [ [TrailAction,X,Y,New_Orient] | Trail ],
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, New_Orient),
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].

turn_right_if_visited(Action, Knowledge) :-
	(not(breeze),not(stench)),
	Action = turnRight,
	haveGold(NGolds),
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),
	shiftOrientTurn(Orient, Action, New_Orient),
	forwardStep(X, Y, New_Orient, New_X, New_Y),
	not(againstWall(X, Y, New_Orient, Max_X, Max_Y)),
	myTrail(Trail),
	visited(Visited),
	not(member([New_X,New_Y], Visited)),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	[[_,_,_,OldOrient] | _ ] = Trail,
	swapTrailTurn(Orient, OldOrient, Action, TrailAction),
	New_Trail = [ [TrailAction,X,Y,New_Orient] | Trail ],
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, New_Orient),
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].


turn_back(Action, Knowledge) :-
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	shiftOrientTurn(Orient, turnLeft, NewOrient),
	myTrail(Trail),
	visited(Visited),
	New_Trail = [ [turnRight,X,Y,Orient] | Trail ], %Orient is misleading here
	Action = turnLeft,
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
	             	haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),	%the position stays the same
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields)].


shiftOrientTurn(west, turnRight, north).
shiftOrientTurn(north, turnRight, east).
shiftOrientTurn(east, turnRight, south).
shiftOrientTurn(south, turnRight, west).
shiftOrientTurn(east, turnLeft, north).
shiftOrientTurn(north, turnLeft, west).
shiftOrientTurn(west, turnLeft, south).
shiftOrientTurn(south, turnLeft, east).
shiftOrientTurn(east, moveForward, east).
shiftOrientTurn(north, moveForward, north).
shiftOrientTurn(west, moveForward, west).
shiftOrientTurn(south, moveForward, south).

swapTrailTurn(Orient, OldOrient, Turn, TrailTurn) :-
	(
		Orient=OldOrient
		-> TrailTurn = Turn
		; ((Turn=turnRight,TrailTurn=turnLeft);(Turn=turnLeft,TrailTurn=turnRight))
	).



appendIfNotMember(NewElement, OldList, NewList) :-
	(
		member(NewElement, OldList)
		-> NewList = OldList
		; NewList = [NewElement | OldList]
	).




updateTrail(Action, Next_X, Next_Y, Orient, OldTrail, NewTrail) :-
	(
		[ [Action, Next_X, Next_Y, Orient] | _ ] = OldTrail
		-> [ _ | NewTrail ] = OldTrail
		; NewTrail = [ [Action, Next_X, Next_Y, Orient] | OldTrail]
	).

updateTrailC(NextPosition, OldTrail, NewTrail) :-
	(
		[ NextPosition | _ ] = OldTrail
		-> [ _ | NewTrail ] = OldTrail
		; NewTrail = [ NextPosition | OldTrail]
	).

getWumpusPosition(StenchFields, Visited, Wumpus_Position) :- 
	[[X, Y] | Tail] = StenchFields,
	getWumpusPosition(Tail, Visited, Wumpus_Position).