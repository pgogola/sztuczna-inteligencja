/* -*- mode: Prolog; comment-column: 48 -*- */

/****************************************************************************
 *
 * Copyright (c) 2013 Witold Paluszynski
 *
 * I grant everyone the right to copy this program in whole or part, and
 * to use it for any purpose, provided the source is properly acknowledged.
 *
 * Udzielam kazdemu prawa do kopiowania tego programu w calosci lub czesci,
 * i wykorzystania go w dowolnym celu, pod warunkiem zacytowania zrodla.
 *
 ****************************************************************************/


/*
  This program implements a simple agent strategy for the wumpus world.
  The agent ignores all dangers of the wumpus world.
  The strategy is to go forward along the perimeter,
  turn left when reached the opposing wall,
  but first of all pick up gold if stumbled upon it,
  and exit the game if at home with gold.
  This version registers all steps on a stack, and uses it to reverse
  the actions after having found gold, thus properly returning home.

  Also demonstrates how to keep track of her own position and orientation.
  The agent assumes that the starting point is (1,1) and orientation "east".
*/

% auxiliary initial action generating rule
act(Action, Knowledge) :-

	% To avoid looping on act/2.
	not(gameStarted),
	assert(gameStarted),

	% Creating initial knowledge
	worldSize(X,Y),				%this is given
	assert(myWorldSize(X,Y)),
	assert(myPosition(1, 1, east)),		%this we assume by default
	assert(myTrail([])),
	assert(visited([])),
	assert(breezeFields([])),
	assert(stenchFields([])),
	assert(wumpusLocation([])),
	assert(haveGold(0)),
	assert(arrows(1)),
	assert(wumpusAlive(true)),
	act(Action, Knowledge).

% standard action generating rules
% this is our agent's algorithm, the rules will be tried in order
act(Action, Knowledge) :- wumpusScream(Action, Knowledge).
act(Action, Knowledge) :- shotToWumpus(Action, Knowledge).
act(Action, Knowledge) :- exit_if_home(Action, Knowledge). %if at home with gold
act(Action, Knowledge) :- exit_if_home_without_money(Action, Knowledge).
act(Action, Knowledge) :- go_back_step(Action, Knowledge). %if have gold elsewhere
act(Action, Knowledge) :- pick_up_gold(Action, Knowledge). %if just found gold
act(Action, Knowledge) :- turn_back_if_breeze_or_stench(Action, Knowledge). %if breeze or stench make one step back
%% act(Action, Knowledge) :- turn_if_wall(Action, Knowledge). %if against the wall
act(Action, Knowledge) :- move_forward_if_not_visited(Action, Knowledge). %if next field has not been visited
act(Action, Knowledge) :- turn_to_not_visited(Action, Knowledge). %turn left or right to new path
act(Action, Knowledge) :- go_backtracking(Action, Knowledge).
act(Action, Knowledge) :- turn_back_if_all_visited(Action, Knowledge).

shotToWumpus(Action, Knowledge) :-
	Action = shoot,
	wumpusAlive(true),
	haveGold(NGolds),
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),
	myTrail(Trail),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	arrows(Arrows),
	[Xw, Yw] = WumpusLocation,
	isInline(X, Y, Orient, Xw, Yw),
	Arrows > 0,
	New_Arrows is (Arrows - 1),
	New_Trail = [ [shoot,X,Y,Orient] | Trail ],
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, Orient),
					myTrail(New_Trail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(New_Arrows),
					wumpusAlive(true)].

wumpusScream(Action, Knowledge) :-
	wumpusAlive(true),
	(
		scream
		-> WympusAlive=false
		; WympusAlive=true
	),
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myTrail(Trail),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	arrows(Arrows),
	Trail = [ [shoot,X,Y,Orient] | Trail_Tail ],
	Action = turnLeft,
	shiftOrient(Orient, Action, NewOrient),
	New_Trail = [ [turnLeft,X,Y,NewOrient] | Trail_Tail ], %Orient is misleading here
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),
					myTrail(New_Trail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WympusAlive)].

exit_if_home(Action, Knowledge) :-
	haveGold(NGolds), NGolds > 0,
	myPosition(1, 1, Orient),
	Action = exit,				%done game
	Knowledge = [].				%irrelevant but required

go_back_step(Action, Knowledge) :-
	%%% assuming we have just found gold:
	%%% 1. our last action must have been grab
	%%% 2. our previuos action must have been moveForward
	%%% 3. so we are initiating a turnback and then return:
	%%%    (a) pop grab from the stack
	%%%    (b) replace it by an artificial turnRight we have never
	%%%        executed, but we will be reversing by turning left
	%%%    (c) execute a turnRight now which together will turn us back
	%%% 4. after that we are facing back and can execute actions in reverse
	%%% 5. because of grab we can be sure this rule is executed exactly once
	haveGold(NGolds), NGolds > 0,
	myWorldSize(Max_X, Max_Y),
	myTrail(Trail),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	Trail = [ [grab,X,Y,Orient] | Trail_Tail ],
	New_Trail = [ [turnRight,X,Y,Orient] | Trail_Tail ], %Orient is misleading here
	Action = turnLeft,
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, Orient),
					myTrail(New_Trail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].
go_back_step(Action, Knowledge) :-
	haveGold(NGolds), NGolds > 0,
	myWorldSize(Max_X, Max_Y),
	myTrail([ [Action,X,Y,Orient] | Trail_Tail ]),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	Action = moveForward,
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, Orient),
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].
%%% backtracking a step can be moving or can be turning
go_back_step(Action, Knowledge) :- go_back_turn(Action, Knowledge).
go_back_turn(Action, Knowledge) :-
	haveGold(NGolds), NGolds > 0,
	myWorldSize(Max_X, Max_Y),
	myTrail([ [OldAct,X,Y,Orient] | Trail_Tail ]),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	%% if our previous action was a turn, we must reverse it now
	((OldAct=turnLeft,Action=turnRight);(OldAct=turnRight,Action=turnLeft)),
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, Orient),
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].
pick_up_gold(Action, Knowledge) :-
	glitter,
	Action = grab,			    %this is easy, we are sitting on it
	haveGold(NGolds),		    %we must know how many golds we have
	NewNGolds is NGolds + 1,
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail(Trail),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	New_Trail = [ [Action,X,Y,Orient] | Trail ], %important to remember grab
	Knowledge = [	gameStarted,
					haveGold(NewNGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, Orient),	%the position stays the same
					myTrail(New_Trail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].
					/*
%% turn_if_wall(Action, Knowledge) :-
%% 	myPosition(X, Y, Orient),
%% 	myWorldSize(Max_X,Max_Y),
%% 	againstWall(X, Y, Orient, Max_X, Max_Y),
%% 	Action = turnLeft,			%always successful
%% 	shiftOrient(Orient, Action, NewOrient),		%always successful
%% 	haveGold(NGolds),
%% 	myTrail(Trail),
%% 	visited(Visited),
%% 	breezeFields(BreezeFields),
%% 	stenchFields(StenchFields),
%% 	New_Trail = [ [Action,X,Y,Orient] | Trail ],
%% 	Knowledge = [	gameStarted,
%% 					haveGold(NGolds),
%% 					myWorldSize(Max_X, Max_Y),
%% 					myPosition(X, Y, NewOrient),
%% 					myTrail(New_Trail),
%% 					visited(Visited),
%% 					breezeFields(BreezeFields),
%% 					stenchFields(StenchFields)].
*/
move_forward_if_not_visited(Action, Knowledge) :-
	Action = moveForward,			%this will fail on a wall
	haveGold(NGolds),
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),
	forwardStep(X, Y, Orient, New_X, New_Y),
	myTrail(Trail),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	not(member([New_X, New_Y], Visited)),
	not(againstWall(X, Y, Orient, Max_X, Max_Y)),
	appendIfNotMember([X, Y], Visited, New_Visited),
	(
		(
			[[OldAction, OldX, OldY, OldOrient] | _ ] = Trail, 
			%% OldAction=moveForward,
			(OldOrient=Orient)
		); 
		[] = Trail
	),
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	%% updateTrailTurn(Trail, X, Y, Orient, UpdatedTrail),
	%% New_Trail = [ [Action,X,Y,Orient] | UpdatedTrail ],
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(New_X, New_Y, Orient),
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].

turn_to_not_visited(Action, Knowledge) :- turn_to_not_visited_left(Action, Knowledge).
turn_to_not_visited(Action, Knowledge) :- turn_to_not_visited_right(Action, Knowledge).

turn_to_not_visited_left(Action, Knowledge) :- 
	wumpusAlive(WumpusAlive),
	Action = turnLeft,
	not(breeze),
	(
		WumpusAlive
		-> not(stench)
		; true
	),
	haveGold(NGolds),
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),
	shiftOrient(Orient, Action, New_Orient),
	forwardStep(X, Y, New_Orient, New_X, New_Y),
	not(againstWall(X, Y, New_Orient, Max_X, Max_Y)),
	myTrail(Trail),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	arrows(Arrows),
	not(member([New_X,New_Y], Visited)),
	(	not(member([New_X,New_Y], Visited));
		againstWall(X, Y, Orient, Max_X, Max_Y)),
	(
		[] = Trail -> (TrailAction=turnRight);
		[[moveForward, _, _, Prev_Orient] | _ ] = Trail,
		swapTrailTurn(Orient, Prev_Orient, Action, TrailAction)
	),
	New_Trail = [ [TrailAction,X,Y,New_Orient] | Trail ],
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, New_Orient),
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].
turn_to_not_visited_right(Action, Knowledge) :-
	wumpusAlive(WumpusAlive),
	Action = turnRight,
	not(breeze),
	(
		WumpusAlive
		-> not(stench)
		; true
	),
	haveGold(NGolds),
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),
	shiftOrient(Orient, Action, New_Orient),
	forwardStep(X, Y, New_Orient, New_X, New_Y),
	not(againstWall(X, Y, New_Orient, Max_X, Max_Y)),
	myTrail(Trail),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	arrows(Arrows),
	not(member([New_X,New_Y], Visited)),
	(	not(member([New_X,New_Y], Visited));
		againstWall(X, Y, Orient, Max_X, Max_Y)),
	(
		[] = Trail -> (TrailAction=turnLeft);
		[[moveForward, _, _, Prev_Orient] | _ ] = Trail,
		swapTrailTurn(Orient, Prev_Orient, Action, TrailAction)
	),
	New_Trail = [ [TrailAction,X,Y,New_Orient] | Trail ],
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, New_Orient),
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].


turn_back_if_breeze_or_stench(Action, Knowledge) :-
	wumpusAlive(WumpusAlive),
	((breeze,Percept1=breeze);Percept1=none),
	((WumpusAlive, stench,Percept2=stench);Percept2=none),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	arrows(Arrows),
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail([ [turnRight,_,_,_] | Trail_Tail ]),
	Action=turnLeft,
	shiftOrient(Orient, Action, NewOrient),
	((Percept1=breeze,member([X, Y], BreezeFields));(WumpusAlive, Percept2=stench,member([X, Y], StenchFields))),
	Knowledge = [	gameStarted,
	             	haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),	%the position stays the same
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].
turn_back_if_breeze_or_stench(Action, Knowledge) :-
	wumpusAlive(WumpusAlive),
	((breeze,Percept1=breeze);Percept1=none),
	((WumpusAlive, stench,Percept2=stench);Percept2=none),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	arrows(Arrows),
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail(Trail),
	not(member([X, Y], BreezeFields)),
	not(member([X, Y], StenchFields)),
	New_Trail = [ [turnRight,X,Y,Orient] | Trail ], %Orient is misleading here
	Action = turnLeft,
	shiftOrient(Orient, Action, NewOrient),
	((Percept1=breeze,appendIfNotMember([X, Y], BreezeFields, New_BreezeFields));New_BreezeFields=BreezeFields),
	((WumpusAlive, Percept2=stench,appendIfNotMember([X, Y], StenchFields, New_StenchFields));New_StenchFields=StenchFields),
	(not(New_BreezeFields=BreezeFields);not(New_StenchFields=StenchFields)),
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
	             	haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),	%the position stays the same
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(New_BreezeFields),
					stenchFields(New_StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].

go_backtracking(Action, Knowledge) :-
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail([ [TrailAction,Trail_X,Trail_Y,Trail_Orient] | Trail_Tail ]),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	forwardStep(X, Y, New_Orient, New_X, New_Y),
	New_X=Trail_X, New_Y=Trail_Y, 
	member([X, Y], Visited),
	Action = moveForward,
	Knowledge = [	gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(New_X, New_Y, Orient),
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].
%%% backtracking a step can be moving or can be turning
go_backtracking(Action, Knowledge) :- go_backtracking_turn(Action, Knowledge).
go_backtracking_turn(Action, Knowledge) :-
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail([ [TrailAction,Trail_X,Trail_Y,Trail_Orient] | Trail_Tail ]),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	%% if our previous action was a turn, we must reverse it now
	((TrailAction=turnLeft,Action=turnRight);(TrailAction=turnRight,Action=turnLeft)),
	shiftOrient(Orient, Action, Next_Orient),
	Knowledge = [	go_backtracking,
					gameStarted,
					haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, Next_Orient),
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].


turn_back_if_all_visited(Action, Knowledge) :-
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail([ [turnRight,_,_,_] | Trail_Tail ]),
	Action=turnLeft,
	shiftOrient(Orient, Action, NewOrient),
	Knowledge = [	gameStarted,
	             	haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),	%the position stays the same
					myTrail(Trail_Tail),
					visited(Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].
turn_back_if_all_visited(Action, Knowledge) :-
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocation),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	haveGold(NGolds),
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail(Trail),
	not(member([X, Y], BreezeFields)),
	not(member([X, Y], StenchFields)),
	New_Trail = [ [turnRight,X,Y,Orient] | Trail ], %Orient is misleading here
	Action = turnLeft,
	shiftOrient(Orient, Action, NewOrient),
	appendIfNotMember([X, Y], Visited, New_Visited),
	Knowledge = [	gameStarted,
	             	haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(X, Y, NewOrient),	%the position stays the same
					myTrail(New_Trail),
					visited(New_Visited),
					breezeFields(BreezeFields),
					stenchFields(StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].

exit_if_home_without_money(Action, Knowledge) :-
	haveGold(NGolds),
	visited(Visited),
	breezeFields(BreezeFields),
	stenchFields(StenchFields),
	wumpusLocation(WumpusLocationKnown),
	wumpusAlive(WumpusAlive),
	arrows(Arrows),
	myWorldSize(Max_X, Max_Y),
	myPosition(1, 1, Orient),
	(	
		([] = WumpusLocationKnown,
		member([2,1], Visited),
		member([1,2], Visited),
		wumpusStench(StenchFields, [], Visited, WumpusLocationsProb, Max_X, Max_Y),
		visitedButNotStench(StenchFields, [], Visited, VisitedNotStench),
		filterBySecondList(WumpusLocationsProb, VisitedNotStench, Wumpus),
		maxOccurances(Wumpus, WumpusLocation),
		length(WumpusLocation, 2),
		New_Visited = [],
		New_BreezeFields = [],
		New_StenchFields = [],
		Action=turnRight,
		shiftOrient(Orient, Action, NewOrient),
		Arrows>0);
		(New_Visited = Visited,
		member([2,1], New_Visited),
		member([1,2], New_Visited),
		WumpusLocation = [],
		Action = exit,
		NewOrient=Orient)
	),
	Knowledge = [	gameStarted,
	             	haveGold(NGolds),
					myWorldSize(Max_X, Max_Y),
					myPosition(1, 1, NewOrient),	%the position stays the same
					myTrail([]),
					visited(New_Visited),
					breezeFields(New_BreezeFields),
					stenchFields(New_StenchFields),
					wumpusLocation(WumpusLocation),
					arrows(Arrows),
					wumpusAlive(WumpusAlive)].

againstWall(X, Y, Orient, Max_X, Max_Y) :- X = Max_X, Orient = east.
againstWall(X, Y, Orient, Max_X, Max_Y) :- Y = 1, Orient = south.
againstWall(X, Y, Orient, Max_X, Max_Y) :- Y = Max_Y, Orient = north.
againstWall(X, Y, Orient, Max_X, Max_Y) :- X = Max_X, Orient = east.
againstWall(X, Y, Orient, Max_X, Max_Y) :- X = 1, Orient = west.
againstWall(X, Y, Orient, Max_X, Max_Y) :- Y = Max_Y, Orient = north.
againstWall(X, Y, Orient, Max_X, Max_Y) :- Y = 1, Orient = south.
againstWall(X, Y, Orient, Max_X, Max_Y) :- X = 1, Orient = west.

shiftOrient(west, turnRight, north).
shiftOrient(north, turnRight, east).
shiftOrient(east, turnRight, south).
shiftOrient(south, turnRight, west).
shiftOrient(east, turnLeft, north).
shiftOrient(north, turnLeft, west).
shiftOrient(west, turnLeft, south).
shiftOrient(south, turnLeft, east).
shiftOrient(east, moveForward, east).
shiftOrient(north, moveForward, north).
shiftOrient(west, moveForward, west).
shiftOrient(south, moveForward, south).

oppositeDirection(west, east).
oppositeDirection(north, south).
oppositeDirection(east, west).
oppositeDirection(south, north).

forwardStep(X, Y, east,  New_X, Y) :- New_X is (X+1).
forwardStep(X, Y, south, X, New_Y) :- New_Y is (Y-1).
forwardStep(X, Y, west,  New_X, Y) :- New_X is (X-1).
forwardStep(X, Y, north, X, New_Y) :- New_Y is (Y+1).

appendIfNotMember(NewElement, OldList, NewList) :-
	(
		member(NewElement, OldList)
		-> NewList = OldList
		; NewList = [NewElement | OldList]
	).

swapTrailTurn(Orient, OldOrient, Turn, TrailTurn) :-
	(
		Orient=OldOrient
		-> TrailTurn = Turn
		; ((Turn=turnRight,TrailTurn=turnLeft);(Turn=turnLeft,TrailTurn=turnRight))
	).


wumpusStench(Stench, Wumpus, Visited, NewWumpus, Max_X, Max_Y) :-
	[] = Stench,
	NewWumpus = Wumpus.

wumpusStench(Stench, Wumpus, Visited, NewWumpus, Max_X, Max_Y) :-
	[[X, Y] | TailStench ] = Stench,
	inc(X, IncX),
	inc(Y, IncY),
	dec(X, DecX),
	dec(Y, DecY),
	((IncX=<Max_X, not(member([IncX, Y], Visited))) -> increaseMemberCounter([IncX, Y], Wumpus, New_Wumpus1);New_Wumpus1=Wumpus),
	((DecX>0, not(member([DecX, Y], Visited))) -> increaseMemberCounter([DecX, Y], New_Wumpus1, New_Wumpus2);New_Wumpus2=New_Wumpus1),
	((IncY=<Max_Y, not(member([X, IncY], Visited))) -> increaseMemberCounter([X, IncY], New_Wumpus2, New_Wumpus3);New_Wumpus3=New_Wumpus2),
	((DecY>0, not(member([X, DecY], Visited))) -> increaseMemberCounter([X, DecY], New_Wumpus3, New_Wumpus4);New_Wumpus4=New_Wumpus3),
	New_Wumpus=New_Wumpus4,
	wumpusStench(TailStench, New_Wumpus, Visited, NewWumpus, Max_X, Max_Y).

increaseMemberCounter(Element, OldList, NewList) :-
	[] = OldList,
	NewList = [[Element, 1] | []].

increaseMemberCounter(Element, OldList, NewList) :-
	[ Head | TailOldList ] = OldList,
	[ Element, Counter ] = Head,
	inc(Counter, CounterInc),
	NewList = [ [Element, CounterInc] | TailOldList ].

increaseMemberCounter(Element, OldList, NewList) :-
	[ Head | TailOldList ] = OldList,
	increaseMemberCounter(Element, TailOldList, RestOfList),
	NewList = [ Head | RestOfList ].

maxOccurances(List, Element) :-
	maxOccurancesHelper(List, 0, [], Element).

maxOccurancesHelper(List, MaxCounter, Element, MaxElement) :-
	[] = List,
	MaxElement = Element.

maxOccurancesHelper(List, MaxCounter, Element, MaxElement) :-
	[[ElementTmp, Counter] | Tail] = List,
	(	Counter>MaxCounter 
		-> maxOccurancesHelper(Tail, Counter, ElementTmp, MaxElement);
		maxOccurancesHelper(Tail, MaxCounter, Element, MaxElement)
	).

isInline(X, Y, Orient, Xw, Yw) :-
	(X = Xw, ((Orient = north, Y<Yw);(Orient = south, Y>Yw)));
	(Y = Yw, ((Orient = east, X<Xw);(Orient = west, X>Xw))).


visitedButNotStench(Stench, VisitedNotStench, Visited, NewVisitedNotStench) :-
	[] = Visited,
	NewVisitedNotStench = VisitedNotStench.

visitedButNotStench(Stench, VisitedNotStench, Visited, NewVisitedNotStench) :-
	[[X, Y] | TailVisited ] = Visited,
	(
		not(member([X, Y], Stench))
		-> 
			inc(X, IncX),
			inc(Y, IncY),
			dec(X, DecX),
			dec(Y, DecY),
			appendIfNotMember([X, IncY], VisitedNotStench, VisitedNotStench1),
			appendIfNotMember([X, DecY], VisitedNotStench1, VisitedNotStench2),
			appendIfNotMember([IncX, Y], VisitedNotStench2, VisitedNotStench3),
			appendIfNotMember([DecX, Y], VisitedNotStench3, VisitedNotStench4)
		; VisitedNotStench4 = VisitedNotStench
	),
	visitedButNotStench(Stench, VisitedNotStench4, TailVisited, NewVisitedNotStench).


filterBySecondList(InputList, FilterList, Result) :-
	[] = InputList,
	Result = [].

filterBySecondList(InputList, FilterList, Result) :-
	[Element | Tail] = InputList,
	filterBySecondList(Tail, FilterList, ResultTmp),
	(
		[Location, _ ] = Element, 
		not(member(Location, FilterList))
		-> Result = [ Element | ResultTmp ]
		; Result = ResultTmp
	).



inc(X, NewX) :- NewX is (X+1).
dec(X, NewX) :- NewX is (X-1).