import random

import numpy as np
from termcolor import colored


def create_empty_board(board_size=(6, 7)):
    return np.zeros(board_size, dtype=np.uint8), np.zeros(board_size[1], dtype=np.uint8)


def insert_element(sign, index, board):
    if board[1][index] >= board[0].shape[0]:
        return None
    new_board = np.array(board[0], order='K', copy=True)
    new_indexes = np.array(board[1], order='K', copy=True)
    new_board[new_indexes[index], index] = sign
    new_indexes[index] += 1
    return new_board, new_indexes


def copy_board(board):
    return np.array(board[0], copy=True), np.array(board[1], copy=True)


def sub_board_generation(board, player):
    boards = []
    for index in range(len(board[1])):
        b = insert_element(player, index, board)
        if not b is None:
            boards.append(b)
    return boards


def min_max(current_board, max_depth, current_player, heuristic_fun, board_generation_fun):
    def min_max_helper(board, depth, min_max_flag, player):
        # check winning
        winner = get_winner(board)
        if 0 != winner:
            return (max_depth - depth + 1) * (1000 if current_player == winner else -1000)
        # current level best scores initialize:
        #       if 'min' then initialize with infinity
        #       if 'max' then initialize with -infinity
        best_scores = float('inf') if "min" == min_max_flag else float('-inf')
        # comparison function:
        #   if next generated board is better than current then return true
        comp_fun = (lambda current, new: current >= new) if "min" == min_max_flag \
            else (lambda current, new: current <= new)
        # generate all supboard steps
        sub_boards = board_generation_fun(board, player)
        if depth >= max_depth or 0 == len(sub_boards):
            return heuristic_fun(board, current_player)
        for b in sub_boards:
            next_scores = min_max_helper(
                copy_board(b), depth + 1, "max" if "min" == min_max_flag else "min", 1 if player == 2 else 2)
            if comp_fun(best_scores, next_scores):
                if best_scores != next_scores:
                    best_scores = next_scores
        return best_scores

    winner = get_winner(current_board)
    if 0 != winner:
        return None, (max_depth + 1) * (1000 if current_player == winner else -1000)
    sub_boards = board_generation_fun(current_board, current_player)
    if 0 == len(sub_boards):
        return None, 0
    next_move = []
    best_scores = float('-inf')
    for b in sub_boards:
        score = min_max_helper(b, 1, "min", 1 if current_player == 2 else 2)
        if best_scores <= score:
            if best_scores != score:
                next_move = []
                best_scores = score
            next_move.append(b)
    return random.choice(next_move) if len(next_move) > 0 else None


def alpha_beta(current_board, max_depth, current_player, heuristic_fun, board_generation_fun):
    def alpha_beta_helper(board, depth, min_max_flag, player, alpha, beta):
        # check winning
        winner = get_winner(board)
        if 0 != winner:
            return (max_depth - depth + 1) * (1000 if current_player == winner else -1000)
        # current level best scores initialize:
        #       if 'min' then initialize with infinity
        #       if 'max' then initialize with -infinity
        best_scores = float('inf') if "min" == min_max_flag else float('-inf')
        # comparison function:
        #   if next generated board is better than current then return true
        comp_fun = (lambda current, new: current >= new) if "min" == min_max_flag \
            else (lambda current, new: current <= new)
        # generate all supboard steps
        sub_boards = board_generation_fun(board, player)
        if depth >= max_depth or 0 == len(sub_boards):
            return heuristic_fun(board, current_player)
        for b in sub_boards:
            next_scores = alpha_beta_helper(
                copy_board(b), depth + 1, "max" if "min" == min_max_flag else "min", 1 if player == 2 else 2, alpha,
                beta)
            if comp_fun(best_scores, next_scores):
                if best_scores != next_scores:
                    best_scores = next_scores
            if 'max' == min_max_flag:
                alpha = max(alpha, best_scores)
            else:
                beta = min(beta, best_scores)
            if alpha >= beta:
                break
        return alpha if min_max_flag == 'max' else beta

    winner = get_winner(current_board)
    if 0 != winner:
        return None, (max_depth + 1) * (1000 if current_player == winner else -1000)
    sub_boards = board_generation_fun(current_board, current_player)
    if 0 == len(sub_boards):
        return None, 0
    next_move = []
    alpha = -float('inf')
    beta = float('inf')
    best_scores = float('-inf')
    for b in sub_boards:
        score = alpha_beta_helper(b, 1, "min", 1 if current_player == 2 else 2, alpha, beta)
        if best_scores <= score:
            if best_scores != score:
                next_move = []
                best_scores = score
            next_move.append(b)
    return random.choice(next_move) if len(next_move) > 0 else None


def get_winner(board):
    if np.all(board[1]==7):
        return -1
    # horizontal
    for i in range(board[0].shape[0]):  # row
        for j in range(board[0].shape[1] - 3):  # column 7-3=4 (bez 4)
            if np.all(board[0][i, j:j + 4] == 2):  # od 3 do 6
                return 2
            elif np.all(board[0][i, j:j + 4] == 1):
                return 1

    # vertical
    for i in range(board[0].shape[0] - 3):  # column 6-3=3 (bez 3)
        for j in range(board[0].shape[1]):
            if np.all(board[0][i:i + 4, j] == 2):  # od 2 do 5
                return 2
            elif np.all(board[0][i:i + 4, j] == 1):
                return 1

    '''
    |  x|   |   |   |
    |   |  x|   |   |
    |   |   |  x|   |
    |   |   |   |  x|
    '''
    for i in range(board[0].shape[0] - 3):  # row
        for j in range(board[0].shape[1] - 3):  # column
            subarray = np.array([board[0][i + c, j + c] for c in range(4)])
            if np.all(subarray == 2):
                return 2
            elif np.all(subarray == 1):
                return 1

    '''
    |   |   |   |  x|
    |   |   |  x|   |
    |   |  x|   |   |
    |  x|   |   |   |
    '''
    for i in range(board[0].shape[0] - 3):  # row
        for j in range(board[0].shape[1] - 1, 2, -1):  # column
            subarray = np.array([board[0][i + c, j - c] for c in range(4)])
            if np.all(subarray == 2):
                return 2
            elif np.all(subarray == 1):
                return 1
    return 0


def print_board(board):
    for i in np.flip(board[0], axis=0):
        board_row = np.array2string(i, separator=" | ").replace('0', ' ').replace('[', '| ').replace(']', ' |')
        for c in board_row:
            if '1' == c:
                print(colored('X', 'green'), end='')
            elif '2' == c:
                print(colored('O', 'red'), end='')
            else:
                print(c, end='')
        print()
    print("-" * 29)
    print(np.array2string(np.arange(1, 8), separator=" | ")
          .replace('[', '| ')
          .replace(']', ' |')
          )
