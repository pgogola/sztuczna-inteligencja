import argparse

import numpy as np

import board


def to_three(board):
    one, two = 0, 0
    # horizontal
    for i in range(board[0].shape[0]):  # row
        for j in range(board[0].shape[1] - 3):  # column
            if np.prod(board[0][i, j:j + 4]) == 0:
                if np.count_nonzero(board[0][i, j:j + 4] == 2) == 3:
                    two += 1
                elif np.count_nonzero(board[0][i, j:j + 4] == 1) == 3:
                    one += 1

    # vertical
    for i in range(board[0].shape[0] - 3):
        for j in range(board[0].shape[1]):
            if np.prod(board[0][i:i + 4, j]) == 0:
                if np.count_nonzero(board[0][i:i + 4, j] == 2) == 3:
                    two += 1
                elif np.count_nonzero(board[0][i:i + 4, j] == 1) == 3:
                    one += 1

    # left_up to right_down
    for i in range(board[0].shape[0] - 3):  # row
        for j in range(board[0].shape[1] - 3):  # column
            subarray = np.array([board[0][i + c, j + c] for c in range(4)])
            if np.prod(subarray) == 0:
                if np.count_nonzero(subarray == 2) == 3:
                    two += 1
                elif np.count_nonzero(subarray == 1) == 3:
                    one += 1

    for i in range(board[0].shape[0] - 3):  # row
        for j in range(board[0].shape[1] - 1, 2, -1):  # column
            subarray = np.array([board[0][i + c, j - c] for c in range(4)])
            if np.prod(subarray) == 0:
                if np.count_nonzero(subarray == 2) == 3:
                    two += 1
                elif np.count_nonzero(subarray == 1) == 3:
                    one += 1
    return one, two


def heur(board, player):
    one, two = to_three(board)
    if player == 1:
        return one - two
    else:
        return two - one


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Opcje gry")
    parser.add_argument('-c', action='store_true', default=False, dest='computer_first', help='rozpoczyna komputer')
    parser.add_argument('--min_max', action='store_true', default=False, dest='algorithm',
                        help='uzywa algorytmu min-max')
    parser.add_argument('-d', action='store', default=6, dest='depth',
                        help='glebokosc przeszukiwania', type=int)
    result = parser.parse_args()
    print(result)
    b = board.create_empty_board()
    isFinish = False
    human = not result.computer_first
    while not isFinish:
        while human:
            board.print_board(b)
            try:
                index = input("Podaj kolumnę (1-7).\nWpisz 'K' zeby komputer wykonał ruch za Ciebie. ")
                if index.lower() == 'k':
                    if result.algorithm:
                        new_b = board.min_max(b, result.depth, 1, heur, board.sub_board_generation)
                    else:
                        new_b = board.alpha_beta(b, result.depth, 1, heur, board.sub_board_generation)
                elif int(index) < 1 or int(index) > 7:
                    human = True
                    continue
                else:
                    new_b = board.insert_element(1, int(index) - 1, b)
            except ValueError:
                print('Niepoprawna wartosc.')
                continue
            if new_b is None:
                human = True
                continue
            human = False
            b = new_b
        winner = board.get_winner(b)
        if winner == -1:
            print("remis")
            exit()
        if 0 != winner:
            board.print_board(b)
            print(f"Wygrales")
            isFinish = True
            continue
        if result.algorithm:
            new_b = board.min_max(b, result.depth, 2, heur, board.sub_board_generation)
        else:
            new_b = board.alpha_beta(b, result.depth, 2, heur, board.sub_board_generation)
        b = new_b
        winner = board.get_winner(b)
        if winner == -1:
            print("remis")
            exit()
        if 0 != winner:
            board.print_board(b)
            winner = board.get_winner(b)
            print(f"Wygral komputer")
            isFinish = True
            continue
        human = True
