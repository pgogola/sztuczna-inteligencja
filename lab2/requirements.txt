autopep8==1.4.4
llvmlite==0.30.0
numba==0.46.0
numpy==1.17.3
pkg-resources==0.0.0
pycodestyle==2.5.0
rope==0.14.0
termcolor==1.1.0
