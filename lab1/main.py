import aiml

products = {
    "chleb duzy": 4,
    "bulka": 0.5,
    "kajzerka": 0.3,
    "chleb maly": 2.5,
    "kawa duza": 4,
    "kawa mala": 2,
    "herbata": 2.5,
    "drozdzowka": 3.5,
    "paczek": 2
}

costs = 0

k = aiml.Kernel()
# k.learn("data.aiml")
k.bootstrap(learnFiles='./data.aiml', commands='load aiml')
while True:
    print(k.respond(input("> ")))
    print(k.getPredicate("username"))
